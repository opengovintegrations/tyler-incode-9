
CREATE VIEW [dbo].[vw_forney_transactions]

AS



SELECT CONCAT(RTRIM(gld_comp COLLATE DATABASE_DEFAULT),' ',gld_cntl COLLATE DATABASE_DEFAULT, gld_year, gld_post, RIGHT('0000' + CAST(gld_padd AS VARCHAR(4)), 4)) AS transaction_key,

CONCAT(RTRIM(gld_comp COLLATE DATABASE_DEFAULT), '-', RTRIM(gld_cntl COLLATE DATABASE_DEFAULT)) AS account_number,

ISNULL(chart.fund_no, '') AS fund_no,

ISNULL(chart.fund_name, '') AS fund_name,

CONCAT(RTRIM(gld_comp COLLATE DATABASE_DEFAULT), '-', RTRIM(gld_cntl COLLATE DATABASE_DEFAULT)) AS control,

ISNULL(chart.object_description, '') AS control_description,

ISNULL(chart.dept_no, '') AS dept_no,

ISNULL(chart.dept_name, '') AS dept_name,

ISNULL(chart.division_no, '') AS division_no,

ISNULL(chart.division_name, '') AS division_name,

ISNULL(chart.category_no, '') AS category_no,

ISNULL(chart.category_name, '') AS category_name,

CASE glc.glc_type

     WHEN 1 THEN 'Asset'

	 WHEN 2 THEN 'Liability'

	 WHEN 3 THEN 'Equity'

	 WHEN 4 THEN 'Revenue'

	 WHEN 5 THEN 'Expenditure'

END AS account_type,

'20' + right('00' + CONVERT(CHAR(2),funds.current_fiscal_year), 2) AS fiscal_year,

CONVERT(DATE, CONCAT(gld.gld_year,

  CASE WHEN gld.gld_date >= 1300 THEN

	CASE WHEN funds.fiscal_month = 2 THEN CONCAT(gld.gld_year, '0228')

	     WHEN funds.fiscal_month IN (1,3,5,7,8,10,12) THEN CONCAT(gld.gld_year, (CONCAT(right ('00' + convert(char(2), funds.fiscal_month), 2)  ,'31')))

		 ELSE CONCAT(gld.gld_year, (CONCAT(right ('00' + convert(char(2), funds.fiscal_month), 2),'30')))

	END

    ELSE

	  right('0000' + convert(varchar(4),gld.gld_date),4)

END)) AS effective_date,

--isdate(concat(gld_year, right('0000' + convert(varchar(4),gld.gld_date),4) ) ),

--gld.gld_year,

--gld.gld_date,

--CONVERT(DATE, CONCAT(gld_year, gld_date)) AS effective_date,

--CONVERT(DATE, CONCAT(gld_year, gld_post)) AS posted_date,

CASE WHEN LEFT(gld_ref,4) = 'CHK:' THEN CONVERT(VARCHAR(6),CONVERT(INT,RIGHT(gld_ref, 6))) ELSE '' END AS check_number,

CASE WHEN LEFT(gld_ref,4) not like 'CHK:' THEN ISNULL(gld_ref, '') ELSE '' END AS reference_text,

ISNULL(gld_desc,'') AS description,

CASE gld_tkey

	WHEN 'A' THEN 'AP'

	WHEN 'P' THEN 'PR'

	WHEN 'B' THEN 'GL'

	WHEN 'C' THEN 'CR'

	ELSE 'GL'

END AS source,

CASE when glc.glc_type IN (2,3,4) THEN gld.gld_amt * -1

	ELSE gld.gld_amt

END AS transaction_amount,

CASE WHEN gld.gld_amt > 0 THEN gld.gld_amt ELSE 0 END AS debit_amt,

CASE WHEN gld.gld_amt < 0 THEN (gld.gld_amt * -1) ELSE 0 END AS credit_amt,

ISNULL(gld.gld_vend,'') AS vendor_number,

ISNULL((SELECT apm_name FROM forney.dbo.APMASTF apm WHERE apm.apm_comp = gld.gld_vco AND apm.apm_vend = gld.gld_vend),'') AS vendor_name,

ISNULL(gld_inv, '') AS invoice_number,

ISNULL(gld_po, '') AS po_number,

gld_enc AS encumbrance,

convert(varchar(10), getdate(), 120) as record_date



FROM forney.dbo.GLDETLF as gld

JOIN forney.dbo.GLCNTLF AS glc ON glc.glc_comp = gld.gld_comp AND glc.glc_cntl = gld.gld_cntl

JOIN forney.dbo.chart ON gld.gld_comp = chart.comp AND gld.gld_cntl = chart.cntl

JOIN forney.dbo.funds ON funds.fund = gld.gld_comp
