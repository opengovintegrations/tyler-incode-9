CREATE VIEW [dbo].[vw_incode_financial_transactions]

AS



SELECT id

      ,t.transaction_key

      ,t.account_number

      ,t.fund_no

      ,t.fund_name

      ,t.control

      ,t.control_description

      ,t.dept_no

      ,t.dept_name

      ,t.division_no

      ,t.division_name

      ,t.category_no

      ,t.category_name

      ,t.account_type

      ,t.fiscal_year

      ,t.je_date

      ,t.post_date

      ,t.check_number

      ,t.reference

      ,t.description

      ,t.source_text

      ,t.transaction_amount

      ,t.debit

      ,t.credit

      ,t.vendor_number

      ,t.vendor_name

      ,t.invoice_number

      ,t.po_number

      ,t.encumbrance

      ,t.record_date

  FROM tbl_incode_financial_transactions t
