CREATE VIEW [dbo].[vw_incode_annual_budgets]

AS



SELECT id

      ,b.account_number

      ,b.fund_no

      ,b.fund_name

      ,b.control

      ,b.control_description

      ,b.dept_no

      ,b.dept_name

      ,b.division_no

      ,b.division_name

      ,b.category_no

      ,b.category_name

      ,b.account_type

      ,b.fiscal_year

      ,b.current_budget

      ,b.original_budget

      ,b.next_year_budget

      ,b.record_date

  FROM tbl_incode_annual_budgets b
